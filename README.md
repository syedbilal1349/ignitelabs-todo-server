## TODO APPLICATION SERVER

# Perequisites

To run the server you need to go in the directory of the project then open the terminal in the respective folder then enter command "npm install" and hit enter. This will install all the dependencies of the project that you need to run it.

# Configuration

To configure your server you can find config.ini is the project directory. This is the file where you can find all the configuration such as Port, Database Credentials etc.

# Run

Open the terminal in the project directory and type "npm start". This will start your server on the port that you have configured in the config.ini file.

# API COLLECTION

You can also find all the API Endpoints in the collection i.e IgniteLabs-TodoAppTask.postman_collection.json in your project directory. You can import this file in "POSTMAN".

Thank You!
