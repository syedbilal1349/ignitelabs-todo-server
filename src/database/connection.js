var mongoose = require('mongoose');
import config from '../conf';

export function connectToDatabase() {
  mongoose.connect('mongodb://' + config.database.host + ':' + config.database.port + '/' + config.database.name, { useNewUrlParser: true, useCreateIndex: true, }).then(resp => {
    if (resp) {
        console.log('Database connected')
    }
    else {
        console.log('Unable to connect to Database')
    }
}).catch(ex => {
    console.log('Unable to connect to Database')
    console.log(ex)
})
}
