var mongoose = require("mongoose");
import config from "../conf/index";
import { findAllTodosOfUser, removeAllTodoOfUser } from "./todo";
const Cryptr = require('cryptr');
var Schema = mongoose.Schema;

// secrect key of encryption
const cryptr = new Cryptr(config.app['cryptr_password']);


export var User = mongoose.model(
  "users",
  new Schema({
    name: String,
    email: String,
    password: String,
    token: String,
    createdAt: String,
    updatedAt: String,
    isActive: { type: Boolean, default: true }
  })
);


export async function saveUser(obj) {
  try {
    let chk = await User.findOne({ email: obj.email, isActive: true })
    if (!chk) {
        let hashVal = cryptr.encrypt(obj.password)
        if (hashVal) {
          obj.password = hashVal
          let userObj = new User(obj)
          let user = await userObj.save()
          return ({ success: true, message: "User successfully registered", data: user })
        }
        else {
          return ({ success: false, message: "Unable to register", data: null })
        }
    } else {
      return ({ success: false, message: "Email address already exist", data: null })
    }
  } catch (err) {
    return ({ success: false, message: "Oops! Error occured", data: err })
  }
}

export async function findAllUser() {
  try {
    let users = await User.find({ isActive: true }).exec()
    if (users) {
      return ({ success: true, message: "Users fetched successfully", data: users })
    }
    else {
      return ({ success: false, message: "No user found", data: null })
    }
  } catch (err) {
    return ({ success: false, message: "Oops! Error occured", data: err })
  }

}

export async function findUserById(id) {
  try {
    let user = await User.findById(id).exec()
    let pass = cryptr.decrypt(user.password)
    user.password = pass
    if (user) {
      return ({
        success: true,
        message: "User fetch successfully",
        data: user
      })
    } else {
      return ({
        success: false,
        message: "No user found",
        data: null
      })
    }
  } catch (err) {
    return ({ success: false, message: "Oops! Error occured", data: err })
  }

}

export async function editUser(id, body) {
  try {
    body.updatedAt = Date.now()
    let user = await User.updateOne(
      { _id: id },
      { $set: body })
    if (user) {
      return ({
        success: true,
        message: "User updated successfully",
        data: null
      })
    } else {
      return ({
        success: false,
        message: "Unable to update user",
        data: null
      })
    }
  } catch (err) {
    return ({ success: false, message: "Oops! Error occured", data: err })
  }

}

export async function deleteUser(userId) {
  try {
    let userTasks = await findAllTodosOfUser(userId)  
    if (userTasks) {
      if (userTasks.length > 0) {
        let deleteTasks = await removeAllTodoOfUser(userId)
        if (deleteTasks) {
          let deletedUser = await User.deleteOne({ _id: userId })
          if (deletedUser) {
            return ({
              success: true,
              message: "User successfully deleted",
              data: null
            })
          } else {
            return ({
              success: false,
              message: "Unable to delete user",
              data: null
            })
          }
        } else {
          return ({
            success: false,
            message: "Unable to delete user, User tasks exist"
          })
        }
      } else {
        let deletedUser = await User.deleteOne({ _id: userId })
        if (deletedUser) {
          return ({
            success: true,
            message: "User successfully deleted",
            data: null
          })
        } else {
          return ({
            success: false,
            message: "Unable to delete user",
            data: null
          })
        }
      }
    } return ({
      success: false,
      message: "Unable to delete user",
      data: null
    })
  } catch (err) {
    return ({ success: false, message: "Oops! Error occured", data: err })
  }
}