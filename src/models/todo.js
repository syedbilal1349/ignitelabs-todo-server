var mongoose = require("mongoose");
var Schema = mongoose.Schema;


export var Todo = mongoose.model(
  "todos",
  new Schema({
    title: String,
    description: String,
    userId: { type: Schema.Types.ObjectId, ref: "users" },
    createdAt: String,
    updatedAt: String,
    isCompleted: { type: Boolean, default: false },
    isActive: { type: Boolean, default: true }
  })
);


export async function saveTodo(obj) {
  try {
    let chk = await Todo.findOne({ title: obj.title })
    if (!chk) {
      obj.createdAt = Date.now()
      let todoObj = new Todo(obj)
      let todo = await todoObj.save()
      if (todo) {
        return ({
          success: true,
          message: "Task created successfully",
          data: todo
        })
      }
      else {
        return ({
          success: false,
          message: "Unable to create task",
          data: null
        })
      }
    } else {
      return ({
        success: false,
        message: "Task already exist",
        data: null
      })
    }
  } catch (err) {
    return ({
      success: false,
      message: "Oops! Error occured",
      data: err
    })
  }
}

export async function findAllTodo() {
  try {
    let todos = await Todo.find({ isActive: true }).populate('userId')
    if (todos) {
      if (todos.length > 0) {
        return ({
          success: true,
          message: "All tasks fetched successfully",
          data: todos
        })
      } else {
        return ({
          success: false,
          message: "No tasks found",
          data: null
        })
      }
    } else {
      return ({
        success: false,
        message: "Unable to fetch tasks",
        data: null
      })
    }
  } catch (err) {
    return ({
      success: false,
      message: "Oops! Error occured",
      data: err
    })
  }
}

export async function findTodoById(id) {
  try {
    let todo = await Todo.findById(id)
    if (todo) {
      return ({
        success: true,
        message: "Task fetched successfully!",
        data: todo
      })
    } else {
      return ({
        success: false,
        message: "No task found",
        data: null
      })
    }
  } catch (err) {
    return ({
      success: false,
      message: "Oops! Error occured",
      data: err
    })
  }
}

export async function editTodo(id, body) {
  try {
    let chk = await Todo.findOne({ title: body.title, isActive: true })
    if (!chk) {
      body.updatedAt = Date.now()
      let todo = await Todo.updateOne(
        { _id: id },
        { $set: body })
      if (todo) {
        return ({
          success: true,
          message: "Task updated successfully",
          data: null
        })
      } else {
        return ({
          success: false,
          message: "Unable to update task",
          data: null
        })
      }
    } else {
      return ({
        success: false,
        message: "Can't update, Task already exist",
        data: null
      })
    }
  } catch (err) {
    return ({
      success: false,
      message: "Oops! Error occured",
      data: err
    })
  }
}

export async function findAllTodosOfUser(userId) {
  try {
    let todos = await Todo.find({ userId: userId, isActive: true }).populate('userId')
    if (todos) {
      if (todos.length > 0) {
        return ({
          success: true,
          message: "All tasks of user fetched successfully",
          data: todos
        })
      } else {
        return ({
          success: false,
          message: "No tasks of user found",
          data: null
        })
      }
    } else {
      return ({
        success: false,
        message: "Unable to fetch tasks of user",
        data: null
      })
    }
  } catch (err) {
    return ({
      success: false,
      message: "Oops! Error occured",
      data: err
    })
  }
}

export async function removeTodo(id) {
  try {
    let deleteTodo = await Todo.deleteOne({ _id: id })
    if (deleteTodo) {
      return ({
        success: true,
        message: "Task removed successfully",
        data: null
      })
    } else {
      return ({
        success: false,
        message: "Unable to remove task",
        data: null
      })
    }
  } catch (err) {
    return ({
      success: false,
      message: "Oops! Error occured",
      data: err
    })
  }

}

export async function removeAllTodoOfUser(userId) {
  try {
    let deleteTodos = await Todo.deleteMany({ userId: userId })
    if (deleteTodos) {
      return ({
        success: true,
        message: "All tasks of user removed successfully",
        data: null
      })
    } else {
      return ({
        success: false,
        message: "Unable to remove user task",
        data: null
      })
    }
  } catch (err) {
    return ({
      success: false,
      message: "Oops! Error occured",
      data: err
    })
  }

}