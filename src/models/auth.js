import config from "../conf/index";
import { sign } from "jsonwebtoken";
import { User } from "./user";
const Cryptr = require('cryptr');

// secrect key of encryption
const cryptr = new Cryptr(config.app['cryptr_password']);

export async function loginUsers(email, password, rememberme) {
    try {
        var expiry = '24h'
        let details = await User.findOne({ email: email })
        if (details) {
            if (rememberme == true) {
                expiry = '1y'
            }
            let plainConvertedPassword = cryptr.decrypt(details.password)
            let validation = plainConvertedPassword == password ? true : false
            if (validation) {
                let tokenObj = {
                    id: details._id,
                    name: details.name,
                    email: details.email,
                }
                let token = await sign(
                    {
                        user: tokenObj
                    },
                    `${config.app["jwtsecret"]}`,
                    {
                        expiresIn: expiry
                    })
                details.token = token
                let updateUser = await User.updateOne({ _id: details._id }, { $set: { token: token } })
                if (updateUser) {
                    return ({ success: true, message: "User LoggedIn Successfully", data: details })
                }
                else {
                    return ({ success: true, message: "Can't Log In", data: null })
                }
            }
            else {
                return ({ success: false, message: "Password Incorrect", data: null })
            }
        }
        else {
            return ({ success: false, message: "No User Found", data: null })
        }
    } catch (err) {
        return ({ success: false, message: "Oops! Error occured", data: err })
    }
}

export async function changePassword(email, oldPassword, newPassword) {
    try {
        let user = await User.findOne({ email: email }).exec()
        if (user) {
            let userPassword = cryptr.decrypt(user.password)
            if (userPassword == oldPassword) {
                newPassword = cryptr.encrypt(newPassword)
                let updatedPassword = await User.updateOne({ email: email }, { $set: { password: newPassword } })
                if (updatedPassword) {
                    return ({ success: true, message: 'Password updated successfully', data: null })
                } else {
                    return ({ success: false, message: 'Can not change password', data: null })
                }
            } else {
                return ({ success: false, message: 'Please enter correct password', data: null })
            }
        } else {
            return ({ success: false, message: 'No user found', data: null })
        }
    } catch (err) {
        return ({ success: false, message: "Oops! Error occured", data: err })
    }

}