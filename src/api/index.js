'use strict';

import { Router } from "express";
import RootAPI from "./root";
import UserAPI from "./user";
import TodoAPI from "./todo";
import AuthAPI from "./auth";



export default class Api {
    constructor(app) {
        this.app = app;
        this.router = Router();
        this.routeGroups = [];
    }

    loadRouteGroups() {
        this.routeGroups.push(new RootAPI());
        this.routeGroups.push(new UserAPI());
        this.routeGroups.push(new TodoAPI());
        this.routeGroups.push(new AuthAPI());

    }

    setContentType(req, resp, next) {
        resp.set('Content-Type', 'text/json');
        next();
    }

    registerGroup() {
        this.loadRouteGroups();
        this.routeGroups.forEach(rg => {
            let setContentType = rg.setContentType ? rg.setContentType : this.setContentType;
            this.app.use('/api' + rg.getRouteGroup(), setContentType, rg.getRouter())
        });
    }
}
