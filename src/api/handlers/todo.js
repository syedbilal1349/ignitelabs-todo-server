"use strict";


import { generateResponse, parseBody } from "../../utilities";
import { saveTodo, findAllTodo, findTodoById, editTodo, removeTodo, findAllTodosOfUser } from "../../models/todo";


export const addTodo = async (req, res) => {
  let body = parseBody(req)
  if (body) {
    body.userId = req.user.id
    body.createdAt = Date.now()
    let newTask = await saveTodo(body)
    generateResponse(newTask.success, newTask.message, newTask.data, res)
  } else {
    generateResponse(false, "Please provide complete info", null, res)
  }
}

export const getAllTodos = async (req, res) => {
  let tasks = await findAllTodo()
  generateResponse(tasks.success, tasks.message, tasks.data, res)
}

export const getAllTodosOfUser = async (req, res) => {
  let userId = req.params.id
  if (userId) {
    let tasks = await findAllTodosOfUser(userId)
    generateResponse(tasks.success, tasks.message, tasks.data, res)
  } else {
    generateResponse(false, "Please provide complete info", null, res)
  }
}

export const getTodoInfo = async (req, res) => {
  let id = req.params.id
  if (id) {
    let tasks = await findTodoById(id)
    generateResponse(tasks.success, tasks.message, tasks.data, res)
  } else {
    generateResponse(false, "Please provide complete info", null, res)
  }
}

export const updateTodo = async (req, res) => {
  let body = parseBody(req)
  let { id } = body
  if (id) {
    let updatedTodo = await editTodo(id, body)
    generateResponse(updatedTodo.success, updatedTodo.message, updatedTodo.data, res)
  } else {
    generateResponse(false, "Please provide complete info", null, res)
  }
}

export const deleteTodo = async (req, res) => {
  let id = req.params.id
  if (id) {
    let deletedTodo = await removeTodo(id)
    generateResponse(deletedTodo.success, deletedTodo.message, deletedTodo.data, res)
  } else {
    generateResponse(false, "Please provide complete info", null, res)
  }
}

