"use strict";


import { generateResponse, parseBody } from "../../utilities";
import { saveUser, findAllUser, editUser, findUserById, deleteUser } from "../../models/user";


export const registerUser = async (req, res) => {
  let body = parseBody(req)
  let { email, password } = body
  if (email && password) {
    body.createdAt = Date.now()
    let newUser = await saveUser(body)
    generateResponse(newUser.success, newUser.message, newUser.data, res)
  } else {
    generateResponse(false, "Please complete the form to register", null, res)
  }
}

export const getAllUsers = async (req, res) => {
  let users = await findAllUser()
  generateResponse(users.success, users.message, users.data, res)
}

export const updateUser = async (req, res) => {
  let body = parseBody(req)
  let { id } = body
  if (id) {
    if (body.password) {
      delete body.password
    }
    let updatedUser = await editUser(id, body)
    generateResponse(updatedUser.success, updatedUser.message, updatedUser.data, res)
  } else {
    generateResponse(false, "Please provide complete info", null, res)
  }
}

export const getUserProfile = async (req, res) => {
  let userId = req.params.id
  if (userId) {
    let userProfile = await findUserById(userId)
    generateResponse(userProfile.success, userProfile.message, userProfile.data, res)
  }
  else {
    generateResponse(false, "Please provide complete info", null, res)
  }
}

export const deleteUserProfle = async (req, res) => {
  let userId = req.user.id
  if (userId) {
    let IsDeleted = await deleteUser(userId)
    generateResponse(IsDeleted.success, IsDeleted.message, IsDeleted.data, res)
  } else {
    generateResponse(false, "Please provide complete info", null, res)
  }
}

