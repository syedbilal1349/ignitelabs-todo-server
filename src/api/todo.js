"use strict";

import { Router } from "express";
import { log, loggedIn } from "./middlewares/index";
import {
  addTodo, getAllTodos, getTodoInfo, updateTodo, deleteTodo, getAllTodosOfUser,
} from "./handlers/todo";

export default class TodoAPI {
  constructor() {
    this.router = Router();
    this.registerRoutes();
  }

  registerRoutes() {
    let router = this.router;
    router.post("/", log, loggedIn, addTodo);
    router.get("/", log, loggedIn, getAllTodos);
    router.get("/:id", log, loggedIn, getTodoInfo);
    router.get("/user/:id", log, loggedIn, getAllTodosOfUser);
    router.put("/", log, loggedIn, updateTodo)
    router.delete("/:id", log, loggedIn, deleteTodo)

  }

  getRouter() {
    return this.router;
  }

  getRouteGroup() {
    return "/todo";
  }
}
