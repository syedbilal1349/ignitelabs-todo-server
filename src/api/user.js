"use strict";

import { Router } from "express";
import { log, loggedIn } from "./middlewares/index";
import {
  registerUser, getAllUsers, getUserProfile, updateUser, deleteUserProfle,
} from "./handlers/user";

export default class UserAPI {
  constructor() {
    this.router = Router();
    this.registerRoutes();
  }

  registerRoutes() {
    let router = this.router;
    router.post("/", log, registerUser);
    router.get("/", log, loggedIn, getAllUsers);
    router.get("/:id", log, loggedIn, getUserProfile);
    router.put("/", log, loggedIn, updateUser)
    router.delete("/:id", log, loggedIn, deleteUserProfle)
  }

  getRouter() {
    return this.router;
  }

  getRouteGroup() {
    return "/user";
  }
}
