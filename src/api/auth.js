"use strict";

import { Router } from "express";
import { log, loggedIn } from "./middlewares/index";
import {
  loginUser, changePasswordOfUser,
} from "./handlers/auth";

export default class AuthAPI {
  constructor() {
    this.router = Router();
    this.registerRoutes();
  }

  registerRoutes() {
    let router = this.router;
    router.put("/login", log, loginUser);
    router.put("/password", log, loggedIn, changePasswordOfUser);
  }

  getRouter() {
    return this.router;
  }

  getRouteGroup() {
    return "/auth";
  }
}
